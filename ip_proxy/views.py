from django.shortcuts import render
from .models import ip_log

# Create your views here.
def index(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    ip_dict = {key: request.headers[key] for key in request.headers.keys()}

    host = request.META.get('REMOTE_HOST')
    server = request.META.get('SERVER_NAME')
    
    record = ip_log(ip_address = ip, isp=server)
    record.save()
    
    return render(request, "ip_test.html", {"ip": ip, "ip_dict":ip_dict, "host":host, "server":server})


def database(request):
    records = ip_log.objects.all()
    return render(request, "ip_database.html",{"records": records})

def delete(request):
    records = ip_log.objects.all().delete()
    return render(request, "ip_database.html",{"records": records})